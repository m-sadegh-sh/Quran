enum SlidableActionType {
  ShareVerse,
  AddVerseToBookmarks,
  RemoveVerseFromBookmarks
}